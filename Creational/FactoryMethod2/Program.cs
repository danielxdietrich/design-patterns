﻿using FactoryMethod2.HiringManagers;

namespace FactoryMethod2
{
    class Program
    {
        static void Main(string[] args)
        {
            var designInterviewer = new DesignManager();
            designInterviewer.TakeInterview();

            var developmentInterviewer = new DevelopmentManager();
            developmentInterviewer.TakeInterview();
        }
    }
}
