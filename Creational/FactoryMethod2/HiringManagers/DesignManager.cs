using FactoryMethod2.Interviewers;

namespace FactoryMethod2.HiringManagers
{
    public class DesignManager : HiringManager
    {
        protected override IInterviewer CreateInterviewer() => new Designer();
    }
}
