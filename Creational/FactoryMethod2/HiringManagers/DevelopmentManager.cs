using FactoryMethod2.Interviewers;

namespace FactoryMethod2.HiringManagers
{
    public class DevelopmentManager : HiringManager
    {
        protected override IInterviewer CreateInterviewer() => new Developer();
    }
}
