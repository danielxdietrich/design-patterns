using static System.Console;

namespace FactoryMethod2.Interviewers
{
    public class Developer : IInterviewer
    {
        public void AskQuestions() => WriteLine("Asking about algorithms.");
    }
}
