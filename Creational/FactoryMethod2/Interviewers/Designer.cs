using static System.Console;

namespace FactoryMethod2.Interviewers
{
    public class Designer : IInterviewer
    {
        public void AskQuestions() => WriteLine("Asking about design principles.");
    }
}
