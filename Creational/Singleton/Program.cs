﻿using System;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            var p1 = President.GetInstance();
            var p2 = President.GetInstance();

            Console.WriteLine(p1 == p2);
        }
    }
}
