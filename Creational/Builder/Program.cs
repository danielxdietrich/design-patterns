﻿using System;

namespace Builder
{
    class Program
    {
        static void Main(string[] args)
        {
            var burger = new BurgerBuilder()
                .AddLettuce()
                .AddPepperoni()
                .AddTomato()
                .Build();

            Console.WriteLine(burger.ToString());
        }
    }
}
