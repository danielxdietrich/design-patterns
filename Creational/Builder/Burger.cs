namespace Builder
{
    public class Burger
    {
        private int Size { get; }
        private bool Cheese { get; }
        private bool Pepperoni { get; }
        private bool Lettuce { get; }
        private bool Tomato { get; }

        public Burger(BurgerBuilder burgerBuilder)
        {
            Size = burgerBuilder.Size;
            Cheese = burgerBuilder.Cheese;
            Pepperoni = burgerBuilder.Pepperoni;
            Lettuce = burgerBuilder.Lettuce;
            Tomato = burgerBuilder.Tomato;
        }

        public override string ToString() => $"A {Size} inch burger.";
    }
}
