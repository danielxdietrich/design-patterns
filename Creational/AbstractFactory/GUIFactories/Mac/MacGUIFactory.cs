using AbstractFactory.GUIElements.Mac;

namespace AbstractFactory.GUIFactories.Mac
{
    public class MacGUIFactory : GUIFactory
    {
        public override MacButton GetButton() => new MacButton();
        public override MacCheckBox GetCheckBox() => new MacCheckBox();
    }
}
