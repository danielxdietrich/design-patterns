using static System.Console;

namespace AbstractFactory.GUIElements.Mac
{
    public class MacButton : IButton
    {
        public void Paint() => WriteLine("Painting Mac Button");
    }
}
