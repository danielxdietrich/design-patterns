using static System.Console;

namespace AbstractFactory.GUIElements.Win
{
    public class WinCheckBox : ICheckBox
    {
        public void Paint() => WriteLine("Painting Windows CheckBox");
    }
}
