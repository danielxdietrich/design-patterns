﻿using System;

namespace AbstractFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            var app = new Application(GenerateOS());

            var button = app.GuiFactory.GetButton();
            var checkBox = app.GuiFactory.GetCheckBox();

            button.Paint();
            checkBox.Paint();
        }

        private static string GenerateOS()
        {
            var useWindows = (new Random().Next(2) == 1);
            return useWindows switch
            {
                true => "win",
                false => "mac",
            };
        }
    }
}
