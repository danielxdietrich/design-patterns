using System;
using FactoryMethod.Trees;

namespace FactoryMethod
{
    public static class TreeFactory
    {
        public static Tree CreateTree(string name) => name.ToLower() switch
        {
            "oak" => new OakTree(),
            "spruce" => new SpruceTree(),
            "birch" => new BirchTree(),
            _ => throw new Exception($"[{name}] tree is not available."),
        };

    }
}
