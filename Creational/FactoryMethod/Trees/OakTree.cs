namespace FactoryMethod.Trees
{
    public class OakTree : Tree
    {
        protected override int MinHeight => 8;
        protected override int MaxHeight => 12;

        public OakTree() : base("Oak") { }
    }
}
