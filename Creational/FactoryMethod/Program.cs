﻿using static System.Console;

namespace FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Write("Enter the name of the tree you wish to create (oak, spruce, birch): ");
            var treeName = ReadLine();

            var tree = TreeFactory.CreateTree(treeName);

            WriteLine(tree.ToString());
        }
    }
}
