namespace Prototype.Shapes
{
    public class Rectangle : Shape
    {
        private double Width { get; }
        private double Height { get; }

        public override double Area => (Width * Height);

        public Rectangle(int x, int y, string color, double width, double height) : base(x, y, color)
        {
            Width = width;
            Height = height;
        }

        private Rectangle(Rectangle rectangle) : base(rectangle)
        {
            Width = rectangle.Width;
            Height = rectangle.Height;
        }

        public override Rectangle Clone() => new(this);
    }
}
