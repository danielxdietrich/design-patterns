using System;

using static System.Console;

namespace Observer
{
    class JobSeeker : IObserver<JobPost>
    {
        public string Name { get; }

        public JobSeeker(string name)
        {
            Name = name;
        }

        public void OnNext(JobPost value)
        {
            WriteLine($"Hi {Name}! New job posted: {value.Title}.");
        }

        public void OnCompleted() { }

        public void OnError(Exception error) { }
    }
}