namespace Observer
{
    struct JobPost
    {
        public string Title { get; }

        public JobPost(string title)
        {
            Title = title;
        }
    }
}