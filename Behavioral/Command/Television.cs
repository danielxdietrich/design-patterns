using static System.Console;

namespace Command
{
    class Television
    {
        public void TurnOn() => WriteLine("TV has been turned on");

        public void TurnOff() => WriteLine("TV has been turned off");
    }
}