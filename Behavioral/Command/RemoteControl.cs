using Command.Commands;

namespace Command
{
    class RemoteControl
    {
        public void Submit(ICommand command) => command.Execute();
    }
}