using Command;

namespace Command.Commands
{
    class TurnOnCommand : ICommand
    {
        private Television television;

        public TurnOnCommand(Television television)
        {
            this.television = television;
        }

        public void Execute() => television.TurnOn();

        public void Undo() => television.TurnOff();
    }
}