﻿using Command.Commands;

namespace Command
{
    class Program
    {
        static void Main(string[] args)
        {
            var tv = new Television();

            var turnOnCommand = new TurnOnCommand(tv);
            var turnOffCommand = new TurnOffCommand(tv);

            var remoteControl = new RemoteControl();

            remoteControl.Submit(turnOnCommand);
            remoteControl.Submit(turnOffCommand);
        }
    }
}
