namespace Iterator
{
    class RadioStation
    {
        public int Frequency { get; }

        public RadioStation(int frequency)
        {
            Frequency = frequency;
        }
    }
}