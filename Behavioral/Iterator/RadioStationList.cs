using System.Collections;
using System.Collections.Generic;

namespace Iterator
{
    class RadioStationList : IEnumerable<RadioStation>
    {
        private List<RadioStation> radioStations = new();

        public RadioStation this[int index]
        {
            get => radioStations[index];
            set => radioStations[index] = value;
        }

        public void Add(RadioStation station)
        {
            radioStations.Add(station);
        }

        public void Remove(RadioStation station)
        {
            radioStations.Remove(station);
        }

        public IEnumerator<RadioStation> GetEnumerator()
        {
            return radioStations.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (var station in radioStations)
            {
                yield return station;
            }
        }
    }
}