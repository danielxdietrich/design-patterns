﻿using static System.Console;

namespace Iterator
{
    class Program
    {
        static void Main(string[] args)
        {
            var radioStations = new RadioStationList();

            var radioStation1 = new RadioStation(98);
            var radioStation2 = new RadioStation(102);
            var radioStation3 = new RadioStation(106);

            radioStations.Add(radioStation1);
            radioStations.Add(radioStation2);
            radioStations.Add(radioStation3);

            foreach (var station in radioStations)
            {
                WriteLine(station.Frequency);
            }
        }
    }
}
