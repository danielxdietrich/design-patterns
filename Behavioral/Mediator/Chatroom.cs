using System;

using static System.Console;

namespace Mediator
{
    class Chatroom : IChatroomMediator
    {
        public void ShowMessage(User user, string message)
        {
            WriteLine($"{DateTime.Now.ToString("H:mm")} [{user.Name}]: {message}");
        }
    }
}