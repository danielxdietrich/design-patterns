﻿using System;

namespace Mediator
{
    class Program
    {
        static void Main(string[] args)
        {
            var mediator = new Chatroom();

            var steve = new User("Steve", mediator);
            var alexa = new User("Alexa", mediator);

            steve.SendMessage("Hello!");
            alexa.SendMessage("Hey!");
        }
    }
}
