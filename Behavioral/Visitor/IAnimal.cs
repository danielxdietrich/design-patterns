namespace Visitor
{
    interface IAnimal
    {
        public string Name { get; }

        void Accept(IAnimalOperation animalOperation);
    }
}