using static System.Console;

namespace Visitor.Animals
{
    class Shark : IAnimal
    {
        public string Name => "Shark";

        public void Bite() => WriteLine($"{Name} is biting.");

        public void Swim() => WriteLine($"{Name} is swimming.");

        public void Accept(IAnimalOperation animalOperation)
        {
            animalOperation.VisitShark(this);
        }
    }
}