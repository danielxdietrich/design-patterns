using static System.Console;

namespace Visitor.Animals
{
    class Eagle : IAnimal
    {
        public string Name => "Eagle";

        public void Fly() => WriteLine($"{Name} is flying.");

        public void Peck() => WriteLine($"{Name} is pecking.");

        public void Accept(IAnimalOperation animalOperation)
        {
            animalOperation.VisitEagle(this);
        }
    }
}