using Visitor.Animals;

namespace Visitor
{
    interface IAnimalOperation
    {
        void VisitEagle(Eagle eagle);
        void VisitLion(Lion lion);
        void VisitShark(Shark dolphin);
    }
}