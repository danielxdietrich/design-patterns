using static System.Console;

namespace Observer2
{
    class Observer : IObserver
    {
        public void Update(ISubject subject)
        {
            WriteLine("Observer: Reacted to the event.");
        }
    }
}