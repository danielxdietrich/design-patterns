﻿using static System.Console;

namespace Observer2
{
    class Program
    {
        static void Main(string[] args)
        {
            var subject = new Subject();

            var observer1 = new Observer();
            var observer2 = new Observer();

            subject.Attach(observer1);
            subject.Attach(observer2);
            WriteLine();

            subject.ChangeState();
            WriteLine();

            subject.ChangeState();
            WriteLine();

            subject.Detach(observer2);
            WriteLine();

            subject.ChangeState();
        }
    }
}
