namespace ChainOfResponsibility.Accounts
{
    class CryptoAccount : Account
    {
        public CryptoAccount(decimal balance) : base(balance) { }
    }
}