using System;

using Bridge.Devices;

namespace Bridge
{
    public class Remote
    {
        private Device device;

        public Remote(Device device)
        {
            this.device = device;
        }

        public void PowerToggle()
        {
            switch (device.IsEnabled)
            {
                case true:
                    device.Disable();
                    break;

                case false:
                    device.Enable();
                    break;
            }
        }

        public void VolumeUp()
        {
            device.Volume++;
            Console.WriteLine($"{device.Name} volume: {device.Volume}");
        }

        public void VolumeDown()
        {
            device.Volume--;
            Console.WriteLine($"{device.Name} volume: {device.Volume}");
        }

        public void ChannelUp()
        {
            device.Channel++;
            Console.WriteLine($"{device.Name} channel: {device.Channel}");
        }

        public void ChannelDown()
        {
            device.Channel--;
            Console.WriteLine($"{device.Name} channel: {device.Channel}");
        }
    }
}
