using System;

namespace Bridge.Devices
{
    public class Radio : Device
    {
        public override string Name { get; } = "Radio";
    }
}
