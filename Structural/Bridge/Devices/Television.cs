using System;

namespace Bridge.Devices
{
    public class Television : Device
    {
        public override string Name { get; } = "Television";
    }
}
