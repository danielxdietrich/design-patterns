using System;

namespace Bridge.Devices
{
    public abstract class Device
    {
        public abstract string Name { get; }
        public bool IsEnabled { get; protected set; }
        public int Volume { get; set; } = 0;
        public int Channel { get; set; } = 0;

        public void Enable()
        {
            IsEnabled = true;
            Console.WriteLine($"{Name} is now enabled");
        }

        public void Disable()
        {
            IsEnabled = false;
            Console.WriteLine($"{Name} is now disabled");
        }
    }
}
