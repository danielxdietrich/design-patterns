﻿using Bridge.Devices;

namespace Bridge
{
    class Program
    {
        static void Main(string[] args)
        {
            var television = new Television();
            var radio = new Radio();

            var tvRemote = new Remote(television);
            tvRemote.PowerToggle();

            var radioRemote = new Remote(radio);
            radioRemote.PowerToggle();
        }
    }
}
