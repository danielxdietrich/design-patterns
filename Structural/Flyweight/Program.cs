﻿using Flyweight.Tea;

namespace Flyweight
{
    class Program
    {
        static void Main(string[] args)
        {
            var teaMaker = new TeaMaker();
            var teaShop = new TeaShop(teaMaker);

            teaShop.TakeOrder("green", 1);
            teaShop.TakeOrder("black", 2);

            teaShop.Serve();
        }
    }
}
