using static System.Console;

namespace Proxy.Doors
{
    class LabDoor : IDoor
    {
        public void Open(string _ = null) => WriteLine("Opening lab door");

        public void Close() => WriteLine("Closing lab door");
    }
}