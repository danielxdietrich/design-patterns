namespace Proxy.Doors
{
    interface IDoor
    {
        void Open(string password = null);
        void Close();
    }
}