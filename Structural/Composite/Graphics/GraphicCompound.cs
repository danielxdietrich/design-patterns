using System.Collections.Generic;

namespace Composite.Graphics
{
    public class GraphicCompound : IGraphic
    {
        public List<IGraphic> Components { get; } = new();

        public void Move(int x, int y)
        {
            foreach (var c in Components)
            {
                c.Move(x, y);
            }
        }

        public void Draw()
        {
            foreach (var c in Components)
            {
                c.Draw();
            }
        }
    }
}
