namespace Composite.Graphics
{
    public interface IGraphic
    {
        public void Move(int x, int y);
        public void Draw();
    }
}
