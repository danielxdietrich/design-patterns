using System;

namespace Composite.Graphics.Components
{
    public abstract class GraphicComponent : IGraphic
    {
        protected (int x, int y) Coordinates;

        public GraphicComponent(int x, int y)
        {
            Coordinates = (x, y);
        }

        public void Draw()
        {
            Console.WriteLine($"Drawing {GetType()} at [{Coordinates.x},{Coordinates.y}]");
        }

        public void Move(int x, int y)
        {
            Coordinates.x += x;
            Coordinates.y += y;
        }
    }
}
