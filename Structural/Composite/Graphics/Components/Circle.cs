namespace Composite.Graphics.Components
{
    public class Circle : Dot
    {
        public int Radius { get; }

        public Circle(int x, int y, int radius) : base(x, y)
        {
            Radius = radius;
        }
    }
}
