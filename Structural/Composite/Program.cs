﻿using Composite.Graphics;
using Composite.Graphics.Components;

namespace Composite
{
    class Program
    {
        static void Main(string[] args)
        {
            var dot = new Dot(10, 20);
            var compound1 = new GraphicCompound();

            compound1.Components.Add(dot);

            var compound2 = new GraphicCompound();
            var circle = new Circle(30, 40, radius: 10);
            var rectangle = new Rectangle(50, 60, width: 10, height: 10);

            compound2.Components.Add(compound1);
            compound2.Components.Add(circle);
            compound2.Components.Add(rectangle);

            compound2.Move(5, 2);
            compound2.Draw();
        }
    }
}
