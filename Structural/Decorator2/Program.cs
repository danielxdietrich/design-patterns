﻿using Decorator2.Coffee;
using Decorator2.Coffee.Decorator;

using static System.Console;

namespace Decorator2
{
    class Program
    {
        static void Main(string[] args)
        {
            var baseCoffee = new BaseCoffee();
            WriteLine(baseCoffee.ToString());

            var milkCoffee = new MilkCoffee(baseCoffee);
            WriteLine(milkCoffee.ToString());

            var vanillaCoffee = new VanillaCoffee(milkCoffee);
            WriteLine(vanillaCoffee.ToString());
        }
    }
}
