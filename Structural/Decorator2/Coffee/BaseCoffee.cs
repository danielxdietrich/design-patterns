namespace Decorator2.Coffee
{
    public class BaseCoffee : ICoffee
    {
        public int Price() => 1;

        public string Description() => $"Coffee";

        public override string ToString() => $"{Description()} costs ${Price()}.";
    }
}
