namespace Decorator2.Coffee.Decorator
{
    public class VanillaCoffee : CoffeeDecorator
    {
        public VanillaCoffee(ICoffee coffee) : base(coffee) { }

        public override int Price() => coffee.Price() + 1;

        public override string Description() => $"{coffee.Description()} & vanilla";

    }
}
