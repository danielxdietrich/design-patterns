namespace Decorator2.Coffee.Decorator
{
    public abstract class CoffeeDecorator : ICoffee
    {
        protected ICoffee coffee;

        public CoffeeDecorator(ICoffee coffee)
        {
            this.coffee = coffee;
        }

        public virtual int Price() => coffee.Price();

        public virtual string Description() => coffee.Description();

        public override string ToString() => $"{Description()} costs ${Price()}.";
    }
}
