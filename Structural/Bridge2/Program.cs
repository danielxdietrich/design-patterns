﻿using Bridge2.Pages;
using Bridge2.Themes;
using System;

namespace Bridge2
{
    class Program
    {
        static void Main(string[] args)
        {
            var contactPage = new PageContact(new ThemeLight());
            Console.WriteLine(contactPage.GetContent());

            var aboutPage = new PageAbout(new ThemeLight());
            Console.WriteLine(aboutPage.GetContent());
        }
    }
}
