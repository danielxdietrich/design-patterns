using Bridge2.Themes;

namespace Bridge2.Pages
{
    public abstract class WebPage
    {
        protected ITheme Theme { get; }
        protected string Title { get; }

        public WebPage(ITheme theme, string title)
        {
            Theme = theme;
            Title = title;
        }

        public string GetContent() => $"{Title} page in {Theme.GetColor()}";
    }
}
