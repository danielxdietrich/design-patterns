using Bridge2.Themes;

namespace Bridge2.Pages
{
    public class PageContact : WebPage
    {
        public PageContact(ITheme theme) : base(theme, "Contact") { }
    }
}
