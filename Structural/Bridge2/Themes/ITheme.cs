namespace Bridge2.Themes
{
    public interface ITheme
    {
        public string GetColor();
    }
}
