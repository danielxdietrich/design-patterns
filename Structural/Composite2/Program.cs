﻿using System;

using Composite2.Employees;

namespace Composite2
{
    class Program
    {
        static void Main(string[] args)
        {
            var organization = new Organization();
            organization.AddEmployee(new Developer("John", 2500));
            organization.AddEmployee(new Designer("Rob", 1000));

            Console.WriteLine($"Net salaries: {organization.GetNetSalaries()}");
        }
    }
}
