using System.Collections.Generic;
using System.Linq;

using Composite2.Employees;

namespace Composite2
{
    class Organization
    {
        private List<IEmployee> Employees { get; } = new();

        public void AddEmployee(IEmployee employee) => Employees.Add(employee);

        public int GetNetSalaries() => Employees.Sum(e => e.Salary);
    }
}
