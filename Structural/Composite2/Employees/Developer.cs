namespace Composite2.Employees
{
    class Developer : IEmployee
    {
        public string Name { get; }
        public string Role { get; } = "Developer";
        public int Salary { get; }

        public Developer(string name, int salary)
        {
            Name = name;
            Salary = salary;
        }
    }
}
