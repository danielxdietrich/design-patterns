namespace Decorator.Data.Decorators
{
    public abstract class DataSourceDecorator : IDataSource
    {
        protected readonly IDataSource dataSource;

        public DataSourceDecorator(IDataSource dataSource)
        {
            this.dataSource = dataSource;
        }

        public virtual void WriteData(string data) => dataSource.WriteData(data);

        public virtual string ReadData() => dataSource.ReadData();
    }
}
